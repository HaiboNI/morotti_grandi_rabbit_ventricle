
#include <cmath>

#include <string>

//  Model of rabbit ventricular myocyte

void InitialiseMorotti_Grandi_Rabbit_Ventricle(double *state) {

	state[0] = 1.0;// dumb state
	state[1] = 0.00136692432434901;
	state[2] = 0.987187785808143;
	state[3] = 0.991851199512730;
	state[4] = 6.97829463600418e-06;
	state[5] = 1.00067553596321;
	state[6] = 0.0272427535166209;
	state[7] = 0.0162143966191208;
	state[8] = 0.00400683744076934;
	state[9] = 0.311269324605836;
	state[10]= 0.00400680852723419;
	state[11] = 0.994640704097414;
	state[12] = 0.00845267773340953;
	state[13] = 0.00619137587968603;
	state[14] = 0.891704198306552;
	state[15] = 1.01075051977649e-06;
	state[16] = 1.22752469230635e-07;
	state[17] = 3.61719944089141;
	state[18] = 0.789244841161171;
	state[19] = 0.00965332026000601;
	state[20] = 0.125119657520623;
	state[21] = 0.00452423634403129;
	state[22] = 0.000323186632488748;
	state[23] = 0.00453661257789703;
	state[24] = 0.134399587892234;
	state[25] = 0.00235053808961292;
	state[26] = 0.00821530042041960;
	state[27] = 0.0105538301445644;
	state[28] = 0.0785044631933100;
	state[29] = 0.120022220911863;
	state[30] = 1.24562627045149;
	state[31] = 0.597809341491486;
	state[32] = 9.15921688880670;
	state[33] = 9.15966523083105;
	state[34] = 9.16009810009052;
	state[35] = 135;
	state[36] = 0.000195415526660400;
	state[37] = 0.000113856153486450;
	state[38] = 9.55237710015368e-05;
	state[39] = -85.7345759237422;
	state[40] = 0.396441759048969;
	state[41] = 0.00136692432434901;
	state[42] = 0.264107873311808;
	state[43] = 0.999537063892383;
	state[44] = 5.32973242015713e-06;
	state[45] = 1.76291981682028e-07;
	state[46] = 2.49312835011359e-06;
	state[47] = 3.01619967712848e-05;
	state[48] = 0.000424241978247493;
	state[49] = 0.999537063892383;
	state[50] = 5.32973242015715e-06;
	state[51] = 1.76291981682020e-07;
	state[52] = 2.49312835011350e-06;
	state[53] = 3.01619967712848e-05;
	state[54] = 0.000424241978247493;
	state[55] = 0.999539199671606;
	state[56] = 5.32974380967928e-06;
	state[57] = 3.52033763846031e-08;
	state[58] = 4.97454193902486e-07;
	state[59] = 3.01620612249497e-05;
	state[60] = 0.000424242884804069;
	state[61] = 0.999539199671606;
	state[62] = 5.32974380968912e-06;
	state[63] = 3.52033763845935e-08;
	state[64] = 4.97454193902354e-07;
	state[65] = 3.01620612249493e-05;
	state[66]  = 0.000424242884804070;
}




// template <typename T >
void Morotti_Grandi_Rabbit_Ventricle(double t, double * y, double *ydot, void * para) {
	//  42 state variables
	// ydot = zeros(size(y));

	//  Assign passed-in parameters
	// double p_HR = para[1];
	double p_HR = 1.0;

	//  Stimulation protocol ('pace' or 'step')
	std::string protocol = "pace";
	// protocol = 'step';

	//  Model Flags
	//  Flag for choice of ICa model (0 or 1)
	int flagMica = 1;
	//  0: HH model for ICa (original model from Shannon et al 2004)
	//  1: Markov model for ICa (and other modifications from Morotti et al 2012)

	//  Flags for EGTA/BAPTA administration (EGTA or BAPTA with 1)
	int flag_EGTA = 0;
	int flag_BAPTA = 0;

	//  Flag for impaired VDI (with 1)
	int flag_redVDI = 0;

	//  Flag for CaM1234 expression (reduced CDI with 1)
	int flag_CaM1234 = 0;

	//  Flag for Ba-Ca substitution (0 or 1)
	int flag_Ba = 0; //  ICa with 0 (default), IBa (and other modifications) with 1

	//  Flag for Ba current model (0 or 1)
	int flag_7_state = 1; //  7-state model with 1 (default), 5-state otherwise

	//  Model Parameters
	//  Constants
	double R = 8314; //  [J/kmol*K]
	double Frdy = 96485; //  [C/mol]
	double Temp = 310; //  [K]
	double FoRT = Frdy / R / Temp; //  [1/mV]
	double Cmem = 1.3810e-10; //  [F] membrane capacitance
	double Qpow = (Temp - 310.0) / 10.0;

	//  Cell geometry
	double cellLength = 100; //  cell length [um]
	double cellRadius = 10.25; //  cell radius [um]
	double junctionLength = 160e-3; //  junc length [um]
	double junctionRadius = 15e-3; //  junc radius [um]
	double distSLcyto = 0.45; //  dist. SL to cytosol [um]
	double distJuncSL = 0.5; //  dist. junc to SL [um]
	double DcaJuncSL = 1.64e-6; //  Dca junc to SL [cm^2/sec]
	double DcaSLcyto = 1.22e-6; //  Dca SL to cyto [cm^2/sec]
	double DnaJuncSL = 1.09e-5; //  Dna junc to SL [cm^2/sec]
	double DnaSLcyto = 1.79e-5; //  Dna SL to cyto [cm^2/sec]
	const double pi = 3.14159265;
	double Vcell = pi * cellRadius * cellRadius * cellLength * 1e-15; //  [L]
	double Vmyo = 0.65 * Vcell;
	double Vsr = 0.035 * Vcell;
	double Vsl = 0.02 * Vcell;
	double Vjunc = 0.0539 * .01 * Vcell;
	double SAjunc = 20150 * pi * 2 * junctionLength * junctionRadius; //  [um^2]
	double SAsl = pi * 2 * cellRadius * cellLength; //  [um^2]
	// J_ca_juncsl = DcaJuncSL*SAjunc/distSLcyto*1e-10;//  [L/msec] = 1.1074e-13
	// J_ca_slmyo = DcaSLcyto*SAsl/distJuncSL*1e-10;  //  [L/msec] = 1.5714e-12
	// J_na_juncsl = DnaJuncSL*SAjunc/distSLcyto*1e-10;//  [L/msec] = 7.36e-13
	// J_na_slmyo = DnaSLcyto*SAsl/distJuncSL*1e-10;  //  [L/msec] = 2.3056e-11
	// J_ca_juncsl = DcaJuncSL*SAjunc/distJuncSL*1e-10;//  [L/msec] = 1.1074e-13
	// J_ca_slmyo = DcaSLcyto*SAsl/distSLcyto*1e-10;  //  [L/msec] = 1.5714e-12
	// J_na_juncsl = DnaJuncSL*SAjunc/distJuncSL*1e-10;//  [L/msec] = 7.36e-13
	// J_na_slmyo = DnaSLcyto*SAsl/distSLcyto*1e-10;  //  [L/msec] = 2.3056e-11
	//  tau's from c-code, not used here
	double J_ca_juncsl = 1 / 1.2134e12; //  [L/msec] = 8.2413e-13
	double J_ca_slmyo = 1 / 2.68510e11; //  [L/msec] = 3.2743e-12
	double J_na_juncsl = 1 / (1.6382e12 / 3 * 100); //  [L/msec] = 6.1043e-13
	double J_na_slmyo = 1 / (1.8308e10 / 3 * 100); //  [L/msec] = 5.4621e-11

	//  Fractional currents in compartments
	double Fjunc = 0.11;
	double Fsl = 1 - Fjunc;
	double Fjunc_CaL = 0.9;
	double Fsl_CaL = 1 - Fjunc_CaL;

	//  Fixed ion concentrations
	double Cli = 15;  //  Intracellular Cl [mM]
	double Clo = 150; //  Extracellular Cl [mM]
	double Ko = 5.4;  //  Extracellular K [mM]
	double Nao = 140; //  Extracellular Na [mM]
	double Cao = 1.8; //  Extracellular Ca [mM]
	double Mgi = 0.5; //  Intracellular Mg [mM]

	//  Nernst Potentials
	double ena_junc = (1 / FoRT) * log(Nao / y[32]); //  [mV]
	double ena_sl = (1 / FoRT) * log(Nao / y[33]); //  [mV]
	double ek = (1 / FoRT) * log(Ko / y[35]);      //  [mV]
	double eca_junc = (1 / FoRT / 2) * log(Cao / y[36]); //  [mV]
	double eca_sl = (1 / FoRT / 2) * log(Cao / y[37]); //  [mV]
	double ecl = (1 / FoRT) * log(Cli / Clo);      //  [mV]

	//  Na transport parameters
	double GNa = 16;
	double GNaL = 0.0045;
	double GNaB = 0.297e-3;    //  [mS/uF]
	double IbarNaK = 1.90719;  //  [uA/uF]
	double KmNaip = 11;        //  [mM]
	double KmKo = 1.5;         //  [mM]
	double Q10NaK = 1.63;
	double Q10KmNai = 1.39;

	//  K current parameters
	double pNaK = 0.01833;
	double GtoSlow = 0.06;     //  [mS/uF]
	double GtoFast = 0.02;     //  [mS/uF]
	double gkp = 0.001;

	//  Cl current parameters
	double GClCa = 0.109625;   //  [mS/uF]
	double GClB = 9e-3;        //  [mS/uF]
	double KdClCa = 100e-3;    //  [mM]

	//  Ca transport parameters
	double Q10CaL = 1.8;
	//  ICaL - HH model
	double pCa = 5.4e-4;       //  [cm/sec]
	double pK = 2.7e-7;        //  [cm/sec]
	double pNa = 1.5e-8;       //  [cm/sec]
	//  ICaL - Markov model
	double Zca = 2;
	double Pca = 100 * 24.3e-6; //  [cm/s] 10*0.45*5.4e-6
	double aff = 1;
	double gammaCai = 0.0341;
	double gammaCao = 0.341;
	double Zk = 1;
	double Pk = 100 * 12.15e-9; //  [cm/s]
	double gammaKi = 0.75;
	double gammaKo = 0.75;
	double Zna = 1;
	double Pna = 100 * 0.675e-9; //  [cm/s]
	double gammaNai = 0.75;
	double gammaNao = 0.75;

	double IbarNCX = 1.0 * 9.0; //  [uA/uF]
	double KmCai = 3.59e-3;    //  [mM]
	double KmCao = 1.3;        //  [mM]
	double KmNai = 12.29;      //  [mM]
	double KmNao = 87.5;       //  [mM]
	double ksat = 0.27;        //  [none]
	double nu = 0.35;          //  [none]
	double Kdact = 0.256e-3;   //  [mM]
	double Q10NCX = 1.57;      //  [none]
	double IbarSLCaP = 0.0673; //  IbarSLCaP FEI changed [uA/uF](2.2 umol/L cytosol/sec) jeff 0.093 [uA/uF]
	double KmPCa = 0.5e-3;     //  [mM]
	double GCaB = 2.513e-4;    //  [uA/uF]
	double Q10SLCaP = 2.35;    //  [none]

	double Q10SRCaP = 2.6;          //  [none]
	double Vmax_SRCaP = 5.3114e-3;  //  [mM/msec] (286 umol/L cytosol/sec)
	double Kmf = 0.246e-3;          //  [mM] default
	// Kmf = 0.175e-3;          //  [mM]
	double Kmr = 1.7;               //  [mM]L cytosol
	double hillSRCaP = 1.787;       //  [mM]
	double ks = 25;                 //  [1/ms]
	double koCa = 10;               //  [mM^-2 1/ms]   // default 10   modified 20
	double kom = 0.06;              //  [1/ms]
	double kiCa = 0.5;              //  [1/mM/ms]
	double kim = 0.005;             //  [1/ms]
	double ec50SR = 0.45;           //  [mM]
	double G_SRleak = 5.348e-6;     //  [1/ms]

	//  Buffering parameters
	double Bmax_Naj = 7.561;       //  [mM] //  Bmax_Naj = 3.7; (c-code difference?)  //  Na buffering
	double Bmax_Nasl = 1.65;       //  [mM]
	double koff_na = 1e-3;         //  [1/ms]
	double kon_na = 0.1e-3;        //  [1/mM/ms]
	double Bmax_TnClow = 70e-3;    //  [mM]                      //  TnC low affinity
	double koff_tncl = 19.6e-3;    //  [1/ms]
	double kon_tncl = 32.7;        //  [1/mM/ms]
	double Bmax_TnChigh = 140e-3;  //  [mM]                      //  TnC high affinity
	double koff_tnchca = 0.032e-3; //  [1/ms]
	double kon_tnchca = 2.37;      //  [1/mM/ms]
	double koff_tnchmg = 3.33e-3;  //  [1/ms]
	double kon_tnchmg = 3e-3;      //  [1/mM/ms]
	double Bmax_CaM = 24e-3;       //  [mM] **? about setting to 0 in c-code**   //  CaM buffering
	double koff_cam = 238e-3;      //  [1/ms]
	double kon_cam = 34;           //  [1/mM/ms]
	double Bmax_myosin = 140e-3;   //  [mM]                      //  Myosin buffering
	double koff_myoca = 0.46e-3;   //  [1/ms]
	double kon_myoca = 13.8;       //  [1/mM/ms]
	double koff_myomg = 0.057e-3;  //  [1/ms]
	double kon_myomg = 0.0157;     //  [1/mM/ms]
	double Bmax_SR = 19 * .9e-3;   //  [mM] (Bers text says 47e-3) 19e-3
	double koff_sr = 60e-3;        //  [1/ms]
	double kon_sr = 100;           //  [1/mM/ms]
	double Bmax_SLlowsl = 37.4e-3 * Vmyo / Vsl;    //  [mM]    //  SL buffering
	double Bmax_SLlowj = 4.6e-3 * Vmyo / Vjunc * 0.1; //  [mM]    // Fei *0.1!!! junction reduction factor
	double koff_sll = 1300e-3;     //  [1/ms]
	double kon_sll = 100;          //  [1/mM/ms]
	double Bmax_SLhighsl = 13.4e-3 * Vmyo / Vsl;   //  [mM]
	double Bmax_SLhighj = 1.65e-3 * Vmyo / Vjunc * 0.1; //  [mM] // Fei *0.1!!! junction reduction factor
	double koff_slh = 30e-3;       //  [1/ms]
	double kon_slh = 100;          //  [1/mM/ms]
	double Bmax_Csqn = 140e-3 * Vmyo / Vsr;        //  [mM] //  Bmax_Csqn = 2.6;      //  Csqn buffering
	double koff_csqn = 65;         //  [1/ms]
	double kon_csqn = 100;         //  [1/mM/ms]
	double redVDI, alpha_CaM1234, aff_m2, flag_7_state_m2;
	//  Parameteres modified in presence of Ba, Ca buffers or mutations
	if (flag_EGTA == 1) {
		//  [Ca]c clamped
		ks = 0; //  no SR Ca release
		Vmax_SRCaP = 0; //  no SR Ca uptake
		G_SRleak = 0; //  no SR Ca leak
	}

	if (flag_BAPTA == 1) {
		//  [Ca]c,sl,j clamped to Cai0 [mM]
		double Cai0 = 1 * 1e-4; //  1*(100 nM) //  DEFINE HERE FREE [Ca]i
		y[36] = Cai0;
		y[37] = Cai0;
		y[38] = Cai0;
		ks = 0; //  no SR Ca release
		Vmax_SRCaP = 0; //  no SR Ca uptake
		G_SRleak = 0; //  no SR Ca leak
	}
	int gks_ca_dep;
	if (flag_Ba == 1) {
		Pca = (1700 / 4200 * 25 / 9 * 0.85) * 100 * 24.3e-6; //  [cm/s]
		aff = 1 / 8;
		//  [Ca]c clamped
		gks_ca_dep = 0; //  no Ca-dep component of IKs conductance
		GClCa = 0; //  no Ca-dep Cl current
		IbarNCX = 0; //  no NCX current
		GCaB = 0; //  no background Ca current
		IbarSLCaP = 0; //  no sarcolemmal Ca pump current
		ks = 0; //  no SR Ca release
		Vmax_SRCaP = 0; //  no SR Ca uptake
		G_SRleak = 0; //  no SR Ca leak
	} else {
		gks_ca_dep = 1;
	}


	if (flag_7_state == 0) {
		aff = 1 / 800000;
		flag_7_state = 1e-15;
	}

	if (flag_redVDI == 1) { //  impaired VDI
		redVDI = 0.30;
	}

	if (flag_CaM1234 == 1) { //  impaired CDI
		alpha_CaM1234 = 0.25; //  DEFINE HERE FRACTION OF MODE-2 LTCCS
		aff_m2 = 1 / 800000;
		flag_7_state_m2 = 1e-15;
	} else {
		alpha_CaM1234 = 0;
		aff_m2 = aff;
		flag_7_state_m2 = flag_7_state;
	}

	//  I_Na: Na Current
	//  Fast I_Na

	double am, bm , ah, bh, aj, bj;
	am = 0.32 * (y[39] + 47.13) / (1 - exp(-0.1 * (y[39] + 47.13)));
	bm = 0.08 * exp(-y[39] / 11);
	if (y[39] >= -40) {
		ah = 0; aj = 0;
		bh = 1 / (0.13 * (1 + exp(-(y[39] + 10.66) / 11.1)));
		bj = 0.3 * exp(-2.535e-7 * y[39]) / (1 + exp(-0.1 * (y[39] + 32)));
	} else {
		ah = 0.135 * exp((80 + y[39]) / -6.8);
		bh = 3.56 * exp(0.079 * y[39]) + 3.1e5 * exp(0.35 * y[39]);
		aj = (-1.2714e5 * exp(0.2444 * y[39]) - 3.474e-5 * exp(-0.04391 * y[39])) * (y[39] + 37.78) / (1 + exp(0.311 * (y[39] + 79.23)));
		bj = 0.1212 * exp(-0.01052 * y[39]) / (1 + exp(-0.1378 * (y[39] + 40.14)));
	}
	ydot[1] = am * (1 - y[1]) - bm * y[1];
	ydot[2] = ah * (1 - y[2]) - bh * y[2];
	ydot[3] = aj * (1 - y[3]) - bj * y[3];

	//  Late I_Na

	double aml, bml, hlinf, tauhl;
	aml = 0.32 * (y[39] + 47.13) / (1 - exp(-0.1 * (y[39] + 47.13))); //  = am
	bml = 0.08 * exp(-y[39] / 11);                        //  = bm
	hlinf = 1 / (1 + exp((y[39] + 91) / 6.1));
	tauhl = 600;
	ydot[41] = aml * (1 - y[41]) - bml * y[41];
	ydot[42] = (hlinf - y[42]) / tauhl;

	double I_NaL_junc = Fjunc * GNaL * y[41] * y[41] * y[41]  * y[42] * (y[39] - ena_junc);
	double I_NaL_sl = Fsl * GNaL * y[41] * y[41] * y[41] * y[42] * (y[39] - ena_sl);
	double I_NaL = I_NaL_junc + I_NaL_sl;

	//  Total I_Na
	double I_Na_junc = Fjunc * GNa * y[1] * y[1] * y[1] * y[2] * y[3] * (y[39] - ena_junc) + I_NaL_junc ;
	double I_Na_sl = Fsl * GNa * y[1] * y[1] * y[1] * y[2] * y[3] * (y[39] - ena_sl) + I_NaL_sl ;
	double I_Natot = I_Na_junc + I_Na_sl;

	//  I_nabk: Na Background Current
	double I_nabk_junc = Fjunc * GNaB * (y[39] - ena_junc);
	double I_nabk_sl = Fsl * GNaB * (y[39] - ena_sl);
	double I_nabk = I_nabk_junc + I_nabk_sl;

	//  I_nak: Na/K Pump Current
	double sigma = (exp(Nao / 67.3) - 1) / 7;
	double fnak = 1 / (1 + 0.1245 * exp(-0.1 * y[39] * FoRT) + 0.0365 * sigma * exp(-y[39] * FoRT));
	double I_nak_junc = Fjunc * IbarNaK * fnak * Ko / (1 + pow((KmNaip / y[32]), 4)) / (Ko + KmKo);
	double I_nak_sl = Fsl * IbarNaK * fnak * Ko / (1 + pow((KmNaip / y[33]), 4)) / (Ko + KmKo);
	double I_nak = I_nak_junc + I_nak_sl;

	//  I_kr: Rapidly Activating K Current
	double gkr = 1 * 0.03 * sqrt(Ko / 5.4);
	double xrss = 1 / (1 + exp(-(y[39] + 50) / 7.5));
	double tauxr = 1 / (1.38e-3 * (y[39] + 7) / (1 - exp(-0.123 * (y[39] + 7))) + 6.1e-4 * (y[39] + 10) / (exp(0.145 * (y[39] + 10)) - 1));
	ydot[12] = (xrss - y[12]) / tauxr;
	double rkr = 1 / (1 + exp((y[39] + 33) / 22.4));
	double I_kr = gkr * y[12] * rkr * (y[39] - ek);

	//  I_ks: Slowly Activating K Current
	double pcaks_junc = -log10(y[36]) + 3.0;
	double pcaks_sl = -log10(y[37]) + 3.0;
	double gks_junc = 0.07 * (0.057 + gks_ca_dep * 0.19 / (1 + exp((-7.2 + pcaks_junc) / 0.6)));
	double gks_sl = 0.07 * (0.057 + gks_ca_dep * 0.19 / (1 + exp((-7.2 + pcaks_sl) / 0.6)));
	double eks = (1 / FoRT) * log((Ko + pNaK * Nao) / (y[35] + pNaK * y[34]));
	double xsss = 1 / (1 + exp(-(y[39] - 1.5) / 16.7));
	double tauxs = 1 / (7.19e-5 * (y[39] + 30) / (1 - exp(-0.148 * (y[39] + 30))) + 1.31e-4 * (y[39] + 30) / (exp(0.0687 * (y[39] + 30)) - 1));
	ydot[13] = (xsss - y[13]) / tauxs;
	double I_ks_junc = Fjunc * gks_junc * y[13] * y[13] * (y[39] - eks);
	double I_ks_sl = Fsl * gks_sl * y[13] * y[13] * (y[39] - eks);
	double I_ks = I_ks_junc + I_ks_sl;

	//  I_kp: Plateau K current
	double kp_kp = 1 / (1 + exp(7.488 - y[39] / 5.98));
	double I_kp_junc = Fjunc * gkp * kp_kp * (y[39] - ek);
	double I_kp_sl = Fsl * gkp * kp_kp * (y[39] - ek);
	double I_kp = I_kp_junc + I_kp_sl;

	//  I_to: Transient Outward K Current (slow and fast components)
	double xtoss = 1 / (1 + exp(-(y[39] + 3.0) / 15));
	double ytoss = 1 / (1 + exp((y[39] + 33.5) / 10));
	double rtoss = 1 / (1 + exp((y[39] + 33.5) / 10));
	double tauxtos = 9 / (1 + exp((y[39] + 3.0) / 15)) + 0.5;
	double tauytos = 3e3 / (1 + exp((y[39] + 60.0) / 10)) + 30;
	// tauytos = 182/(1+exp((y[39]+33.5)/10))+1;
	double taurtos = 2.8e3 / (1 + exp((y[39] + 60.0) / 10)) + 220; // Fei changed here!! time-dependent gating variable
	// taurtos = 8085/(1+exp((y[39]+33.5)/10))+313;
	ydot[8] = (xtoss - y[8]) / tauxtos;
	ydot[9] = (ytoss - y[9]) / tauytos;
	ydot[40] = (rtoss - y[40]) / taurtos; // Fei changed here!! time-dependent gating variable
	double I_tos = GtoSlow * y[8] * (y[9] + 0.5 * y[40]) * (y[39] - ek); //  [uA/uF]

	double tauxtof = 3.5 * exp(-y[39] * y[39] / 30 / 30) + 1.5;
	// tauxtof = 3.5*exp(-((y[39]+3)/30)^2)+1.5;
	double tauytof = 20.0 / (1 + exp((y[39] + 33.5) / 10)) + 20.0;
	// tauytof = 20.0/(1+exp((y[39]+33.5)/10))+20.0;
	ydot[10] = (xtoss - y[10]) / tauxtof;
	ydot[11] = (ytoss - y[11]) / tauytof;
	double I_tof = GtoFast * y[10] * y[11] * (y[39] - ek);

	double I_to = I_tos + I_tof;

	//  I_ki: Time-Independent K Current
	double aki = 1.02 / (1 + exp(0.2385 * (y[39] - ek - 59.215)));
	double bki = (0.49124 * exp(0.08032 * (y[39] + 5.476 - ek)) + exp(0.06175 * (y[39] - ek - 594.31))) / (1 + exp(-0.5143 * (y[39] - ek + 4.753)));
	double kiss = aki / (aki + bki);
	double I_ki = 1 * 0.9 * sqrt(Ko / 5.4) * kiss * (y[39] - ek);

	//  I_ClCa: Ca-activated Cl Current, I_Clbk: background Cl Current
	double I_ClCa_junc = Fjunc * GClCa / (1 + KdClCa / y[36]) * (y[39] - ecl);
	double I_ClCa_sl = Fsl * GClCa / (1 + KdClCa / y[37]) * (y[39] - ecl);
	double I_ClCa = I_ClCa_junc + I_ClCa_sl;
	double I_Clbk = GClB * (y[39] - ecl);

	//  I_Ca: L-type Calcium Current (HH model, flagMica=0)
	double dss = 1 / (1 + exp(-(y[39] + 14.5) / 6.0));
	double taud = dss * (1 - exp(-(y[39] + 14.5) / 6.0)) / (0.035 * (y[39] + 14.5));
	double fss = 1 / (1 + exp((y[39] + 35.06) / 3.6)) + 0.6 / (1 + exp((50 - y[39]) / 20));
	double tauf = 1 / (0.0197 * exp( -(0.0337 * (y[39] + 14.5)) * (0.0337 * (y[39] + 14.5)) ) + 0.02);
	ydot[4] = (dss - y[4]) / taud;
	ydot[5] = (fss - y[5]) / tauf;
	ydot[6] = 1.7 * y[36] * (1 - y[6]) - 11.9e-3 * y[6]; //  fCa_junc   koff!!!!!!!!
	ydot[7] = 1.7 * y[37] * (1 - y[7]) - 11.9e-3 * y[7]; //  fCa_sl
	// double fcaCaMSL = 0.1 / (1 + (0.01 / y[37]));  // take care
	// double fcaCaj = 0.1 / (1 + (0.01 / y[36]));
	double fcaCaMSL = 0;
	double fcaCaj = 0;
	// y[6]=0;
	// y[7]=0;
	double ibarca_j = pCa * 4 * (y[39] * Frdy * FoRT) * (0.341 * y[36] * exp(2 * y[39] * FoRT) - 0.341 * Cao) / (exp(2 * y[39] * FoRT) - 1);
	double ibarca_sl = pCa * 4 * (y[39] * Frdy * FoRT) * (0.341 * y[37] * exp(2 * y[39] * FoRT) - 0.341 * Cao) / (exp(2 * y[39] * FoRT) - 1);
	double ibark = pK * (y[39] * Frdy * FoRT) * (0.75 * y[35] * exp(y[39] * FoRT) - 0.75 * Ko) / (exp(y[39] * FoRT) - 1);
	double ibarna_j = pNa * (y[39] * Frdy * FoRT) * (0.75 * y[32] * exp(y[39] * FoRT) - 0.75 * Nao)  / (exp(y[39] * FoRT) - 1);
	double ibarna_sl = pNa * (y[39] * Frdy * FoRT) * (0.75 * y[33] * exp(y[39] * FoRT) - 0.75 * Nao)  / (exp(y[39] * FoRT) - 1);

	double I_Ca_junc1 = (Fjunc_CaL * ibarca_j * y[4] * y[5] * ((1 - y[6]) + fcaCaj) * pow(Q10CaL, Qpow)) * 0.45 * 1;
	double I_Ca_sl1 = (Fsl_CaL * ibarca_sl * y[4] * y[5] * ((1 - y[7]) + fcaCaMSL) * pow(Q10CaL, Qpow)) * 0.45 * 1;
	// I_Ca = I_Ca_junc+I_Ca_sl;
	double I_CaK1 = (ibark * y[4] * y[5] * (Fjunc_CaL * (fcaCaj + (1 - y[6])) + Fsl_CaL * (fcaCaMSL + (1 - y[7]))) * pow(Q10CaL, Qpow)) * 0.45 * 1;
	double I_CaNa_junc1 = (Fjunc_CaL * ibarna_j * y[4] * y[5] * ((1 - y[6]) + fcaCaj) * pow(Q10CaL, Qpow)) * 0.45 * 1;
	double I_CaNa_sl1 = (Fsl_CaL * ibarna_sl * y[4] * y[5] * ((1 - y[7]) + fcaCaMSL) * pow(Q10CaL, Qpow)) * .45 * 1;
	// I_CaNa = I_CaNa_junc+I_CaNa_sl;
	// I_Catot = I_Ca+I_CaK+I_CaNa;

	//  I_Ca: L-type Calcium Current (Markov model, flagMica=1)
	//  LTCC Current - Input: state variables (12-24)
	//  junc, mode-1
	double Pc2_LCCj_m1 = y[43];
	double Pc1_LCCj_m1 = y[44];
	double Pi1Ca_LCCj_m1 = y[45];
	double Pi2Ca_LCCj_m1 = y[46];
	double Pi1Ba_LCCj_m1 = y[47];
	double Pi2Ba_LCCj_m1 = y[48];
	//  junc, mode-2
	double Pc2_LCCj_m2 = y[49];
	double Pc1_LCCj_m2 = y[50];
	double Pi1Ca_LCCj_m2 = y[51];
	double Pi2Ca_LCCj_m2 = y[52];
	double Pi1Ba_LCCj_m2 = y[53];
	double Pi2Ba_LCCj_m2 = y[54];
	//  SL, mode-1
	double Pc2_LCCsl_m1 = y[55];
	double Pc1_LCCsl_m1 = y[56];
	double Pi1Ca_LCCsl_m1 = y[57];
	double Pi2Ca_LCCsl_m1 = y[58];
	double Pi1Ba_LCCsl_m1 = y[59];
	double Pi2Ba_LCCsl_m1 = y[60];
	//  SL, mode-2
	double Pc2_LCCsl_m2 = y[61];
	double Pc1_LCCsl_m2 = y[62];
	double Pi1Ca_LCCsl_m2 = y[63];
	double Pi2Ca_LCCsl_m2 = y[64];
	double Pi1Ba_LCCsl_m2 = y[65];
	double Pi2Ba_LCCsl_m2 = y[66];
	//  dependent state-variables (PO)
	double Po_LCCj_m1 =  1 - Pc2_LCCj_m1 - Pc1_LCCj_m1 - Pi1Ca_LCCj_m1 - Pi2Ca_LCCj_m1 - Pi1Ba_LCCj_m1 - Pi2Ba_LCCj_m1;
	double Po_LCCj_m2 =  1 - Pc2_LCCj_m2 - Pc1_LCCj_m2 - Pi1Ca_LCCj_m2 - Pi2Ca_LCCj_m2 - Pi1Ba_LCCj_m2 - Pi2Ba_LCCj_m2;
	double Po_LCCsl_m1 = 1 - Pc2_LCCsl_m1 - Pc1_LCCsl_m1 - Pi1Ca_LCCsl_m1 - Pi2Ca_LCCsl_m1 - Pi1Ba_LCCsl_m1 - Pi2Ba_LCCsl_m1;
	double Po_LCCsl_m2 = 1 - Pc2_LCCsl_m2 - Pc1_LCCsl_m2 - Pi1Ca_LCCsl_m2 - Pi2Ca_LCCsl_m2 - Pi1Ba_LCCsl_m2 - Pi2Ba_LCCsl_m2;

	//  LTCC Current - Input: intracellular concentrations
	double cajLCC = y[36];
	double kjLCC = y[35];
	double najLCC = y[32];
	double caslLCC = y[37];
	double kslLCC = y[35];
	double naslLCC = y[33];

	//  LTCC Current - Temp-dependent Parameters
	double ICa_scale = 1 * pow(Q10CaL, Qpow);
	double ICa_speed = 1;

	//  LTCC Current - Fixed Parameters
	double cpt = 3.75e-3;     //  [mM]
	double cat = 7.617e-3;    //  [mM]
	double s1o = 0.0182688;   //  [1/ms]
	double k1o = 0.024168;    //  [1/ms]
	double k2o = 0.000103615; //  [1/ms]
	double sp0 = 1.5;
	double sp1 = 3;           //  [ms]
	double sp2 = 40;          //  [mV]
	double sp3 = 3;           //  [mV]
	double sp4 = 4;           //  [mV]
	double sp5 = 11.32;       //  [mV]
	double sp6 = 15.6;        //  [mV]
	double sp7 = 10;          //  [ms]
	double sp8 = 4954;        //  [ms]
	double sp9 = 78.0329;     //  [ms]
	double sp10 = 0.1;        //  [ms]
	double aR2 = 1;
	double sR2 = -2;          //  [mV]
	double pR2 = 0.145;       //  [1/mV]
	double aT2 = 1;           //  [1/ms]
	double sT2 = -1000;       //  [mV]
	double pT2 = 0.100;       //  [1/mV]
	double aR1 = 0.09091;
	double sR1 = -1000;       //  [mV]
	double pR1 = 0.100;       //  [1/mV]
	double aT1 = 0.30303;     //  [1/ms]
	double sT1 = -1000;       //  [mV]
	double pT1 = 0.100;       //  [1/mV]
	double aRv2 = 0.9;
	double sRv2 = -29;        //  [mV]
	double pRv2 = 0.135;      //  [1/mV]
	double aTv2 = 500;        //  [1/ms]
	double sTv2 = -25;        //  [mV]
	double pTv2 = 0.050;      //  [1/mV]
	double aRv1 = 0.85;
	double sRv1 = -180;       //  [mV]
	double pRv1 = 0.090;      //  [1/mV]
	double aTv1 = 270;        //  [1/ms]
	double sTv1 = -180;       //  [mV]
	double pTv1 = 0.100;      //  [1/mV]
	double aTrev1 = 205.12;   //  [1/ms]
	double sTrev1 = -65;      //  [mV]
	double pTrev1 = 0.100;    //  [1/mV]
	double aTrev2 = 7e8;      //  [1/ms]
	double sTrev2 = 60;       //  [mV]
	double pTrev2 = 0.130;    //  [1/mV]

	if (flag_redVDI == 1) {
		aRv2 = redVDI * aRv2;
		aRv1 = redVDI * aRv1;
	}

	//  junc // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
	//  I_Ca JUNC - mode-1
	//  Voltage- and Ca-dependent Parameters
	double fcp = 1 / (1 + pow((cpt / cajLCC / aff), 3)); //  Ca-dep
	double tca = sp9 / (1 + pow((cajLCC * aff / cat), 4)) + sp10; //  Ca-dep
	double R2 = aR2 / (1 + exp(-(y[39] - sR2) * pR2));
	double T2 = aT2 / (1 + exp(-(y[39] - sT2) * pT2));
	double PT = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp3)));
	double R1 = aR1 / (1 + exp(-(y[39] - sR1) * pR1));
	double T1 = aT1 / (1 + exp(-(y[39] - sT1) * pT1));
	double RV = sp7 + sp8 * exp(y[39] / sp6);
	double Pr = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp4)));
	double Pq = 1 + sp0 / (1 + exp(-(y[39] + sp2) / sp4));
	double TCa = Pq * ((RV - tca) * Pr + tca);
	double Ps = 1 / (1 + exp(-(y[39] + sp2) / sp5));
	double Rv1 = aRv1 / (1 + exp(-(y[39] - sRv1) * pRv1));
	double Tv1 = aTv1 / (1 + exp(-(y[39] - sTv1) * pTv1));
	double Rv2 = aRv2 / (1 + exp(-(y[39] - sRv2) * pRv2));
	double Tv2 = aTv2 / (1 + exp(-(y[39] - sTv2) * pTv2));
	double Trev1 = aTrev1 / (1 + exp(-(y[39] - sTrev1) * pTrev1));
	double Frev1 = (1 - Rv1) / Rv1 * R1 / (1 - R1);
	double Trev2 = aTrev2 / (1 + exp(-(y[39] - sTrev2) * pTrev2));
	double Frev2 = (1 - Rv2) / Rv2 * R2 / (1 - R2) * Rv1 / (1 - Rv1);
	//  Transition Rates (20 rates)
	double alphaLCC = ICa_speed * R2 / T2;
	double betaLCC = ICa_speed * (1 - R2) / T2;
	double r1 = ICa_speed * R1 / T1;
	double r2 = ICa_speed * (1 - R1) / T1;
	double k1 = flag_7_state * ICa_speed * k1o * fcp;
	double k2 = ICa_speed * k2o;
	double k3 = ICa_speed * PT / sp1;
	// k4
	double k5 = ICa_speed * (1 - Ps) / TCa;
	double k6 = flag_7_state * ICa_speed * fcp * Ps / TCa;
	double s1 = flag_7_state * ICa_speed * s1o * fcp;
	// s2
	double k1p = ICa_speed * Rv1 / Tv1;
	double k2p = ICa_speed * (1 - Rv1) / Tv1;
	double k3p = ICa_speed * 1 / (Trev2 * (1 + Frev2));
	double k4p = Frev2 * k3p;                        //  REV
	double k5p = ICa_speed * (1 - Rv2) / Tv2;
	double k6p = ICa_speed * Rv2 / Tv2;
	double s1p = ICa_speed * 1 / (Trev1 * (1 + Frev1));
	double s2p = Frev1 * s1p;                        //  REV
	double k4 = k3 * (alphaLCC / betaLCC) * (k1 / k2) * (k5 / k6); //  REV
	double s2 = s1 * (k2 / k1) * (r1 / r2);          //  REV

	//  State transitions for mode-1 junctional LCCs
	double dPc2_LCCj_m1 = betaLCC * Pc1_LCCj_m1 + k5 * Pi2Ca_LCCj_m1 + k5p * Pi2Ba_LCCj_m1 - (k6 + k6p + alphaLCC) * Pc2_LCCj_m1;          //  C2_m1j
	double dPc1_LCCj_m1 = alphaLCC * Pc2_LCCj_m1 + k2 * Pi1Ca_LCCj_m1 + k2p * Pi1Ba_LCCj_m1 + r2 * Po_LCCj_m1 - (r1 + betaLCC + k1 + k1p) * Pc1_LCCj_m1; //  C1_m1j
	double dPi1Ca_LCCj_m1 = k1 * Pc1_LCCj_m1 + k4 * Pi2Ca_LCCj_m1 + s1 * Po_LCCj_m1 - (k2 + k3 + s2) * Pi1Ca_LCCj_m1;                  //  I1Ca_m1j
	double dPi2Ca_LCCj_m1 = k3 * Pi1Ca_LCCj_m1 + k6 * Pc2_LCCj_m1 - (k4 + k5) * Pi2Ca_LCCj_m1;                                         //  I2Ca_m1j
	double dPi1Ba_LCCj_m1 = k1p * Pc1_LCCj_m1 + k4p * Pi2Ba_LCCj_m1 + s1p * Po_LCCj_m1 - (k2p + k3p + s2p) * Pi1Ba_LCCj_m1;            //  I1Ba_m1j
	double dPi2Ba_LCCj_m1 = k3p * Pi1Ba_LCCj_m1 + k6p * Pc2_LCCj_m1 - (k5p + k4p) * Pi2Ba_LCCj_m1;                                     //  I2Ba_m1j

	double ibarca_jm1 = Zca * Zca * Pca * Frdy * FoRT * y[39] / (exp(Zca * y[39] * FoRT) - 1) * (gammaCai * cajLCC * exp(Zca * y[39] * FoRT) - gammaCao * Cao);
	double I_Ca_junc_m1 = Fjunc_CaL * (ibarca_jm1 * Po_LCCj_m1) * ICa_scale;

	double ibarna_jm1 = Zna * Zna * Pna * Frdy * FoRT * y[39] / (exp(Zna * y[39] * FoRT) - 1) * (gammaNai * najLCC * exp(Zna * y[39] * FoRT) - gammaNao * Nao);
	double I_Na_junc_m1 = Fjunc_CaL * (ibarna_jm1 * Po_LCCj_m1) * ICa_scale;

	double ibark_jm1 = Zk * Zk * Pk * Frdy * FoRT * y[39] / (exp(Zk * y[39] * FoRT) - 1) * (gammaKi * kjLCC * exp(Zk * y[39] * FoRT) - gammaKo * Ko);
	double I_K_junc_m1 = Fjunc_CaL * (ibark_jm1 * Po_LCCj_m1) * ICa_scale;

	//  I_Ca JUNC - mode-2 (channels expressing CaM1234, impaired CDI)
	//  Re-define all parameters as mode-2 specific parameters
	//  Voltage- and Ca-dependent Parameters
	fcp = 1 / (1 + pow((cpt / cajLCC / aff_m2), 3)); //  Ca-dep
	tca = sp9 / (1 + pow((cajLCC * aff_m2 / cat), 4)) + sp10; //  Ca-dep
	R2 = aR2 / (1 + exp(-(y[39] - sR2) * pR2));
	T2 = aT2 / (1 + exp(-(y[39] - sT2) * pT2));
	PT = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp3)));
	R1 = aR1 / (1 + exp(-(y[39] - sR1) * pR1));
	T1 = aT1 / (1 + exp(-(y[39] - sT1) * pT1));
	RV = sp7 + sp8 * exp(y[39] / sp6);
	Pr = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp4)));
	Pq = 1 + sp0 / (1 + exp(-(y[39] + sp2) / sp4));
	TCa = Pq * ((RV - tca) * Pr + tca);
	Ps = 1 / (1 + exp(-(y[39] + sp2) / sp5));
	Rv1 = aRv1 / (1 + exp(-(y[39] - sRv1) * pRv1));
	Tv1 = aTv1 / (1 + exp(-(y[39] - sTv1) * pTv1));
	Rv2 = aRv2 / (1 + exp(-(y[39] - sRv2) * pRv2));
	Tv2 = aTv2 / (1 + exp(-(y[39] - sTv2) * pTv2));
	Trev1 = aTrev1 / (1 + exp(-(y[39] - sTrev1) * pTrev1));
	Frev1 = (1 - Rv1) / Rv1 * R1 / (1 - R1);
	Trev2 = aTrev2 / (1 + exp(-(y[39] - sTrev2) * pTrev2));
	Frev2 = (1 - Rv2) / Rv2 * R2 / (1 - R2) * Rv1 / (1 - Rv1);
	//  Re-calculate transition rates
	double alphaLCCm2 = ICa_speed * R2 / T2;
	double betaLCCm2 = ICa_speed * (1 - R2) / T2;
	double r1m2 = ICa_speed * R1 / T1;
	double r2m2 = ICa_speed * (1 - R1) / T1;
	double k1m2 = flag_7_state_m2 * ICa_speed * k1o * fcp;
	double k2m2 = ICa_speed * k2o;
	double k3m2 = ICa_speed * PT / sp1;
	// k4
	double k5m2 = ICa_speed * (1 - Ps) / TCa;
	double k6m2 = flag_7_state_m2 * ICa_speed * fcp * Ps / TCa;
	double s1m2 = flag_7_state_m2 * ICa_speed * s1o * fcp;
	// s2
	double k1pm2 = ICa_speed * Rv1 / Tv1;
	double k2pm2 = ICa_speed * (1 - Rv1) / Tv1;
	double k3pm2 = ICa_speed * 1 / (Trev2 * (1 + Frev2));
	double k4pm2 = Frev2 * k3pm2;                                    //  REV
	double k5pm2 = ICa_speed * (1 - Rv2) / Tv2;
	double k6pm2 = ICa_speed * Rv2 / Tv2;
	double s1pm2 = ICa_speed * 1 / (Trev1 * (1 + Frev1));
	double s2pm2 = Frev1 * s1pm2;                                    //  REV
	double k4m2 = k3m2 * (alphaLCCm2 / betaLCCm2) * (k1m2 / k2m2) * (k5m2 / k6m2); //  REV
	double s2m2 = s1m2 * (k2m2 / k1m2) * (r1m2 / r2m2);              //  REV

	//  State transitions for mode-2 junctional LCCs
	double dPc2_LCCj_m2 = betaLCCm2 * Pc1_LCCj_m2 + k5m2 * Pi2Ca_LCCj_m2 + k5pm2 * Pi2Ba_LCCj_m2 - (k6m2 + k6pm2 + alphaLCCm2) * Pc2_LCCj_m2;              //  C2_m2j
	double dPc1_LCCj_m2 = alphaLCCm2 * Pc2_LCCj_m2 + k2m2 * Pi1Ca_LCCj_m2 + k2pm2 * Pi1Ba_LCCj_m2 + r2m2 * Po_LCCj_m2 - (r1m2 + betaLCCm2 + k1m2 + k1pm2) * Pc1_LCCj_m2; //  C1_m2j
	double dPi1Ca_LCCj_m2 = k1m2 * Pc1_LCCj_m2 + k4m2 * Pi2Ca_LCCj_m2 + s1m2 * Po_LCCj_m2 - (k2m2 + k3m2 + s2m2) * Pi1Ca_LCCj_m2;                      //  I1Ca_m2j
	double dPi2Ca_LCCj_m2 = k3m2 * Pi1Ca_LCCj_m2 + k6m2 * Pc2_LCCj_m2 - (k4m2 + k5m2) * Pi2Ca_LCCj_m2;                                                 //  I2Ca_m2j
	double dPi1Ba_LCCj_m2 = k1pm2 * Pc1_LCCj_m2 + k4pm2 * Pi2Ba_LCCj_m2 + s1pm2 * Po_LCCj_m2 - (k2pm2 + k3pm2 + s2pm2) * Pi1Ba_LCCj_m2;                //  I1Ba_m2j
	double dPi2Ba_LCCj_m2 = k3pm2 * Pi1Ba_LCCj_m2 + k6pm2 * Pc2_LCCj_m2 - (k5pm2 + k4pm2) * Pi2Ba_LCCj_m2;                                             //  I2Ba_m2j

	double ibarca_jm2 = Zca * Zca * Pca * Frdy * FoRT * y[39] / (exp(Zca * y[39] * FoRT) - 1) * (gammaCai * cajLCC * exp(Zca * y[39] * FoRT) - gammaCao * Cao);
	double I_Ca_junc_m2 = Fjunc_CaL * (ibarca_jm2 * Po_LCCj_m2) * ICa_scale;

	double ibarna_jm2 = Zna * Zna * Pna * Frdy * FoRT * y[39] / (exp(Zna * y[39] * FoRT) - 1) * (gammaNai * najLCC * exp(Zna * y[39] * FoRT) - gammaNao * Nao);
	double I_Na_junc_m2 = Fjunc_CaL * (ibarna_jm2 * Po_LCCj_m2) * ICa_scale;

	double ibark_jm2 = Zk * Zk * Pk * Frdy * FoRT * y[39] / (exp(Zk * y[39] * FoRT) - 1) * (gammaKi * kjLCC * exp(Zk * y[39] * FoRT) - gammaKo * Ko);
	double I_K_junc_m2 = Fjunc_CaL * (ibark_jm2 * Po_LCCj_m2) * ICa_scale;

	// junc_mode2 = 0; //  Sum up total fraction of CKII and PKA-shifted mode 2 channels
	double junc_mode2 = alpha_CaM1234;

	//  Total junctional I_Ca
	double I_Ca_junc2 = (1 - junc_mode2) * I_Ca_junc_m1 + junc_mode2 * I_Ca_junc_m2;
	double I_Na_junc2 = (1 - junc_mode2) * I_Na_junc_m1 + junc_mode2 * I_Na_junc_m2;
	double I_K_junc2 = (1 - junc_mode2) * I_K_junc_m1 + junc_mode2 * I_K_junc_m2;
	//  // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

	//  SL // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
	//  I_Ca SL - mode-1
	//  Voltage- and Ca-dependent Parameters
	fcp = 1 / (1 + pow ((cpt / caslLCC / aff), 3)); //  Ca-dep
	tca = ((sp9 / (1 + pow ((caslLCC * aff / cat), 4))) + sp10); //  Ca-dep //  /1000
	R2 = aR2 / (1 + exp(-(y[39] - sR2) * pR2));
	T2 = aT2 / (1 + exp(-(y[39] - sT2) * pT2));
	PT = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp3)));
	R1 = aR1 / (1 + exp(-(y[39] - sR1) * pR1));
	T1 = aT1 / (1 + exp(-(y[39] - sT1) * pT1));
	RV = sp7 + sp8 * exp(y[39] / sp6);
	Pr = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp4)));
	Pq = 1 + sp0 / (1 + exp(-(y[39] + sp2) / sp4));
	TCa = Pq * ((RV - tca) * Pr + tca);
	Ps = 1 / (1 + exp(-(y[39] + sp2) / sp5));
	Rv1 = aRv1 / (1 + exp(-(y[39] - sRv1) * pRv1));
	Tv1 = aTv1 / (1 + exp(-(y[39] - sTv1) * pTv1));
	Rv2 = aRv2 / (1 + exp(-(y[39] - sRv2) * pRv2));
	Tv2 = aTv2 / (1 + exp(-(y[39] - sTv2) * pTv2));
	Trev1 = aTrev1 / (1 + exp(-(y[39] - sTrev1) * pTrev1));
	Frev1 = (1 - Rv1) / Rv1 * R1 / (1 - R1);
	Trev2 = aTrev2 / (1 + exp(-(y[39] - sTrev2) * pTrev2));
	Frev2 = (1 - Rv2) / Rv2 * R2 / (1 - R2) * Rv1 / (1 - Rv1);
	//  Transition Rates (20 rates)
	alphaLCC = ICa_speed * R2 / T2;
	betaLCC = ICa_speed * (1 - R2) / T2;
	r1 = ICa_speed * R1 / T1;
	r2 = ICa_speed * (1 - R1) / T1;
	k1 = flag_7_state * ICa_speed * k1o * fcp;
	k2 = ICa_speed * k2o;
	k3 = ICa_speed * PT / sp1;
	// k4
	k5 = ICa_speed * (1 - Ps) / TCa;
	k6 = flag_7_state * ICa_speed * fcp * Ps / TCa;
	s1 = flag_7_state * ICa_speed * s1o * fcp;
	// s2
	k1p = ICa_speed * Rv1 / Tv1;
	k2p = ICa_speed * (1 - Rv1) / Tv1;
	k3p = ICa_speed * 1 / (Trev2 * (1 + Frev2));
	k4p = Frev2 * k3p;                        //  REV
	k5p = ICa_speed * (1 - Rv2) / Tv2;
	k6p = ICa_speed * Rv2 / Tv2;
	s1p = ICa_speed * 1 / (Trev1 * (1 + Frev1));
	s2p = Frev1 * s1p;                        //  REV
	k4 = k3 * (alphaLCC / betaLCC) * (k1 / k2) * (k5 / k6); //  REV
	s2 = s1 * (k2 / k1) * (r1 / r2);          //  REV

	//  State transitions for 'mode 1' sarcolemmal LCCs
	double dPc2_LCCsl_m1 = betaLCC * Pc1_LCCsl_m1 + k5 * Pi2Ca_LCCsl_m1 + k5p * Pi2Ba_LCCsl_m1 - (k6 + k6p + alphaLCC) * Pc2_LCCsl_m1;          //  C2_m1sl
	double dPc1_LCCsl_m1 = alphaLCC * Pc2_LCCsl_m1 + k2 * Pi1Ca_LCCsl_m1 + k2p * Pi1Ba_LCCsl_m1 + r2 * Po_LCCsl_m1 - (r1 + betaLCC + k1 + k1p) * Pc1_LCCsl_m1; //  C1_m1sl
	double dPi1Ca_LCCsl_m1 = k1 * Pc1_LCCsl_m1 + k4 * Pi2Ca_LCCsl_m1 + s1 * Po_LCCsl_m1 - (k2 + k3 + s2) * Pi1Ca_LCCsl_m1;             //  I1Ca_m1sl
	double dPi2Ca_LCCsl_m1 = k3 * Pi1Ca_LCCsl_m1 + k6 * Pc2_LCCsl_m1 - (k4 + k5) * Pi2Ca_LCCsl_m1;                                       //  I2Ca_m1sl
	double dPi1Ba_LCCsl_m1 = k1p * Pc1_LCCsl_m1 + k4p * Pi2Ba_LCCsl_m1 + s1p * Po_LCCsl_m1 - (k2p + k3p + s2p) * Pi1Ba_LCCsl_m1;           //  I1Ba_m1sl
	double dPi2Ba_LCCsl_m1 = k3p * Pi1Ba_LCCsl_m1 + k6p * Pc2_LCCsl_m1 - (k5p + k4p) * Pi2Ba_LCCsl_m1;                                       //  I2Ba_m1sl

	double ibarca_slm1 = Zca * Zca * Pca * Frdy * FoRT * y[39] / (exp(Zca * y[39] * FoRT) - 1) * (gammaCai * caslLCC * exp(Zca * y[39] * FoRT) - gammaCao * Cao);
	double I_Ca_sl_m1 = Fsl_CaL * (ibarca_slm1 * Po_LCCsl_m1) * ICa_scale;

	double ibarna_slm1 = Zna * Zna * Pna * Frdy * FoRT * y[39] / (exp(Zna * y[39] * FoRT) - 1) * (gammaNai * naslLCC * exp(Zna * y[39] * FoRT) - gammaNao * Nao);
	double I_Na_sl_m1 = Fsl_CaL * (ibarna_slm1 * Po_LCCsl_m1) * ICa_scale;

	double ibark_slm1 = Zk * Zk * Pk * Frdy * FoRT * y[39] / (exp(Zk * y[39] * FoRT) - 1) * (gammaKi * kslLCC * exp(Zk * y[39] * FoRT) - gammaKo * Ko);
	double I_K_sl_m1 = Fsl_CaL * (ibark_slm1 * Po_LCCsl_m1) * ICa_scale;

	//  I_Ca SL - mode-2 (channels expressing CaM1234, impaired CDI)
	//  Re-define all parameters as mode-2 specific parameters
	fcp = 1 / (1 + pow((cpt / caslLCC / aff_m2), 3)); //  Ca-dep
	tca = sp9 / (1 + pow((caslLCC * aff_m2 / cat), 4)) + sp10; //  Ca-dep
	R2 = aR2 / (1 + exp(-(y[39] - sR2) * pR2));
	T2 = aT2 / (1 + exp(-(y[39] - sT2) * pT2));
	PT = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp3)));
	R1 = aR1 / (1 + exp(-(y[39] - sR1) * pR1));
	T1 = aT1 / (1 + exp(-(y[39] - sT1) * pT1));
	RV = sp7 + sp8 * exp(y[39] / sp6);
	Pr = 1 - (1 / (1 + exp(-(y[39] + sp2) / sp4)));
	Pq = 1 + sp0 / (1 + exp(-(y[39] + sp2) / sp4));
	TCa = Pq * ((RV - tca) * Pr + tca);
	Ps = 1 / (1 + exp(-(y[39] + sp2) / sp5));
	Rv1 = aRv1 / (1 + exp(-(y[39] - sRv1) * pRv1));
	Tv1 = aTv1 / (1 + exp(-(y[39] - sTv1) * pTv1));
	Rv2 = aRv2 / (1 + exp(-(y[39] - sRv2) * pRv2));
	Tv2 = aTv2 / (1 + exp(-(y[39] - sTv2) * pTv2));
	Trev1 = aTrev1 / (1 + exp(-(y[39] - sTrev1) * pTrev1));
	Frev1 = (1 - Rv1) / Rv1 * R1 / (1 - R1);
	Trev2 = aTrev2 / (1 + exp(-(y[39] - sTrev2) * pTrev2));
	Frev2 = (1 - Rv2) / Rv2 * R2 / (1 - R2) * Rv1 / (1 - Rv1);
	//  Re-calculate transition rates
	alphaLCCm2 = ICa_speed * R2 / T2;
	betaLCCm2 = ICa_speed * (1 - R2) / T2;
	r1m2 = ICa_speed * R1 / T1;
	r2m2 = ICa_speed * (1 - R1) / T1;
	k1m2 = flag_7_state_m2 * ICa_speed * k1o * fcp;
	k2m2 = ICa_speed * k2o;
	k3m2 = ICa_speed * PT / sp1;
	// k4
	k5m2 = ICa_speed * (1 - Ps) / TCa;
	k6m2 = flag_7_state_m2 * ICa_speed * fcp * Ps / TCa;
	s1m2 = flag_7_state_m2 * ICa_speed * s1o * fcp;
	// s2
	k1pm2 = ICa_speed * Rv1 / Tv1;
	k2pm2 = ICa_speed * (1 - Rv1) / Tv1;
	k3pm2 = ICa_speed * 1 / (Trev2 * (1 + Frev2));
	k4pm2 = Frev2 * k3pm2;                                    //  REV
	k5pm2 = ICa_speed * (1 - Rv2) / Tv2;
	k6pm2 = ICa_speed * Rv2 / Tv2;
	s1pm2 = ICa_speed * 1 / (Trev1 * (1 + Frev1));
	s2pm2 = Frev1 * s1pm2;                                    //  REV
	k4m2 = k3m2 * (alphaLCCm2 / betaLCCm2) * (k1m2 / k2m2) * (k5m2 / k6m2); //  REV
	s2m2 = s1m2 * (k2m2 / k1m2) * (r1m2 / r2m2);              //  REV

	//  State transitions for mode-2 sarcolemmal LCCs
	double dPc2_LCCsl_m2 = betaLCCm2 * Pc1_LCCsl_m2 + k5m2 * Pi2Ca_LCCsl_m2 + k5pm2 * Pi2Ba_LCCsl_m2 - (k6m2 + k6pm2 + alphaLCCm2) * Pc2_LCCsl_m2;          //  C2_m2sl
	double dPc1_LCCsl_m2 = alphaLCCm2 * Pc2_LCCsl_m2 + k2m2 * Pi1Ca_LCCsl_m2 + k2pm2 * Pi1Ba_LCCsl_m2 + r2m2 * Po_LCCsl_m2 - (r1m2 + betaLCCm2 + k1m2 + k1pm2) * Pc1_LCCsl_m2; //  C1_m2sl
	double dPi1Ca_LCCsl_m2 = k1m2 * Pc1_LCCsl_m2 + k4m2 * Pi2Ca_LCCsl_m2 + s1m2 * Po_LCCsl_m2 - (k2m2 + k3m2 + s2m2) * Pi1Ca_LCCsl_m2;           //  I1Ca_m2sl
	double dPi2Ca_LCCsl_m2 = k3m2 * Pi1Ca_LCCsl_m2 + k6m2 * Pc2_LCCsl_m2 - (k4m2 + k5m2) * Pi2Ca_LCCsl_m2;                                       //  I2Ca_m2sl
	double dPi1Ba_LCCsl_m2 = k1pm2 * Pc1_LCCsl_m2 + k4pm2 * Pi2Ba_LCCsl_m2 + s1pm2 * Po_LCCsl_m2 - (k2pm2 + k3pm2 + s2pm2) * Pi1Ba_LCCsl_m2;         //  I1Ba_m2sl
	double dPi2Ba_LCCsl_m2 = k3pm2 * Pi1Ba_LCCsl_m2 + k6pm2 * Pc2_LCCsl_m2 - (k5pm2 + k4pm2) * Pi2Ba_LCCsl_m2;                                       //  I2Ba_m2sl

	double ibarca_slm2 = Zca * Zca * Pca * Frdy * FoRT * y[39] / (exp(Zca * y[39] * FoRT) - 1) * (gammaCai * caslLCC * exp(Zca * y[39] * FoRT) - gammaCao * Cao);
	double I_Ca_sl_m2 = Fsl_CaL * (ibarca_slm2 * Po_LCCsl_m2) * ICa_scale;

	double ibarna_slm2 = Zna * Zna * Pna * Frdy * FoRT * y[39] / (exp(Zna * y[39] * FoRT) - 1) * (gammaNai * naslLCC * exp(Zna * y[39] * FoRT) - gammaNao * Nao);
	double I_Na_sl_m2 = Fsl_CaL * (ibarna_slm2 * Po_LCCsl_m2) * ICa_scale;

	double ibark_slm2 = Zk * Zk * Pk * Frdy * FoRT * y[39] / (exp(Zk * y[39] * FoRT) - 1) * (gammaKi * kslLCC * exp(Zk * y[39] * FoRT) - gammaKo * Ko);
	double I_K_sl_m2 = Fsl_CaL * (ibark_slm2 * Po_LCCsl_m2) * ICa_scale;

	// sl_mode2 = 0; //  Sum up total fraction of CKII and PKA-shifted mode 2 channels
	double sl_mode2 = alpha_CaM1234;

	//  Total junctional I_Ca
	double I_Ca_sl2 = (1 - sl_mode2) * I_Ca_sl_m1 + sl_mode2 * I_Ca_sl_m2;
	double I_Na_sl2 = (1 - sl_mode2) * I_Na_sl_m1 + sl_mode2 * I_Na_sl_m2;
	double I_K_sl2 = (1 - sl_mode2) * I_K_sl_m1 + sl_mode2 * I_K_sl_m2;
	//  // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

	//  Total K current through LCC
	double I_CaK2 = I_K_junc2 + I_K_sl2;

	//  output LTCC module ODE
	// ydot(43: 48) = [dPc2_LCCj_m1 dPc1_LCCj_m1 dPi1Ca_LCCj_m1 dPi2Ca_LCCj_m1 dPi1Ba_LCCj_m1 dPi2Ba_LCCj_m1];
	// ydot(49: 54) = [dPc2_LCCj_m2 dPc1_LCCj_m2 dPi1Ca_LCCj_m2 dPi2Ca_LCCj_m2 dPi1Ba_LCCj_m2 dPi2Ba_LCCj_m2];
	// ydot(55: 60) = [dPc2_LCCsl_m1 dPc1_LCCsl_m1 dPi1Ca_LCCsl_m1 dPi2Ca_LCCsl_m1 dPi1Ba_LCCsl_m1 dPi2Ba_LCCsl_m1];
	// ydot(61: 66) = [dPc2_LCCsl_m2 dPc1_LCCsl_m2 dPi1Ca_LCCsl_m2 dPi2Ca_LCCsl_m2 dPi1Ba_LCCsl_m2 dPi2Ba_LCCsl_m2];
	// ydot(43: 48) = [
	ydot[43] = dPc2_LCCj_m1;
	ydot[44] = dPc1_LCCj_m1;
	ydot[45] = dPi1Ca_LCCj_m1;
	ydot[46] = dPi2Ca_LCCj_m1;
	ydot[47] = dPi1Ba_LCCj_m1;
	ydot[48] = dPi2Ba_LCCj_m1;
	ydot[49] = dPc2_LCCj_m2;
	ydot[50] = dPc1_LCCj_m2;
	ydot[51] = dPi1Ca_LCCj_m2;
	ydot[52] = dPi2Ca_LCCj_m2;
	ydot[53] = dPi1Ba_LCCj_m2;
	ydot[54] = dPi2Ba_LCCj_m2;
	ydot[55] = dPc2_LCCsl_m1;
	ydot[56] = dPc1_LCCsl_m1;
	ydot[57] = dPi1Ca_LCCsl_m1;
	ydot[58] = dPi2Ca_LCCsl_m1;
	ydot[59] = dPi1Ba_LCCsl_m1;
	ydot[60] = dPi2Ba_LCCsl_m1;
	ydot[61] = dPc2_LCCsl_m2;
	ydot[62] = dPc1_LCCsl_m2;
	ydot[63] = dPi1Ca_LCCsl_m2;
	ydot[64] = dPi2Ca_LCCsl_m2;
	ydot[65] = dPi1Ba_LCCsl_m2;
	ydot[66] = dPi2Ba_LCCsl_m2;


	//  Compute total I_Ca (depending on flagMica)
	double I_Ca_junc = (1 - flagMica) * I_Ca_junc1 + flagMica * I_Ca_junc2;
	double I_Ca_sl = (1 - flagMica) * I_Ca_sl1 + flagMica * I_Ca_sl2;
	double I_Ca = I_Ca_junc + I_Ca_sl; //  Total Ca curren throuhgh LCC

	double I_CaK = (1 - flagMica) * (I_CaK1) + flagMica * (I_CaK2); //  Total K current through LCC

	double I_CaNa_junc = (1 - flagMica) * (I_CaNa_junc1) + (flagMica) * (I_Na_junc2);
	double I_CaNa_sl = (1 - flagMica) * (I_CaNa_sl1) + (flagMica) * (I_Na_sl2);
	double I_CaNa = I_CaNa_junc + I_CaNa_sl; //  Total Na current through LCC

	//  Collect all currents through LCC
	double I_Catot = I_Ca + I_CaK + I_CaNa;

	//  I_ncx: Na/Ca Exchanger flux
	double Ka_junc = 1 / (1 + pow((Kdact / y[36]), 3));
	double Ka_sl = 1 / (1 + pow((Kdact / y[37]), 3));
	double s1_junc = exp(nu * y[39] * FoRT) * y[32] * y[32] * y[32] * Cao;
	double s1_sl = exp(nu * y[39] * FoRT) * y[33] * y[33] * y[33]  * Cao;
	double s2_junc = exp((nu - 1) * y[39] * FoRT) * Nao * Nao * Nao * y[36];
	double s3_junc = KmCai * Nao * Nao * Nao   * (1 + pow((y[32] / KmNai), 3)) + KmNao * KmNao * KmNao * y[36] * (1 + y[36] / KmCai) + KmCao * y[32] * y[32] * y[32] + y[32] * y[32] * y[32] * Cao + Nao * Nao * Nao * y[36];
	double s2_sl = exp((nu - 1) * y[39] * FoRT) * Nao * Nao * Nao * y[37];
	double s3_sl = KmCai * Nao * Nao * Nao * (1 + pow((y[33] / KmNai), 3)) + KmNao * KmNao * KmNao * y[37] * (1 + y[37] / KmCai) + KmCao * y[33] * y[33] * y[33] + y[33] * y[33] * y[33] * Cao + Nao * Nao * Nao * y[37];
	double I_ncx_junc = Fjunc * IbarNCX * pow(Q10NCX, Qpow) * Ka_junc * (s1_junc - s2_junc) / s3_junc / (1 + ksat * exp((nu - 1) * y[39] * FoRT));
	double I_ncx_sl = Fsl * IbarNCX * pow(Q10NCX, Qpow) * Ka_sl * (s1_sl - s2_sl) / s3_sl / (1 + ksat * exp((nu - 1) * y[39] * FoRT));
	double I_ncx = I_ncx_junc + I_ncx_sl;

	//  I_pca: Sarcolemmal Ca Pump Current
	double I_pca_junc = Fjunc * pow(Q10SLCaP, Qpow) * IbarSLCaP * pow(y[36], 1.6) / (pow(KmPCa, 1.6) + pow(y[36], 1.6));
	double I_pca_sl = Fsl * pow(Q10SLCaP, Qpow) * IbarSLCaP * pow(y[37], 1.6) / (pow(KmPCa, 1.6) + pow(y[37], 1.6));
	double I_pca = I_pca_junc + I_pca_sl;

	//  I_cabk: Ca Background Current
	double I_cabk_junc = Fjunc * GCaB * (y[39] - eca_junc);
	double I_cabk_sl = Fsl * GCaB * (y[39] - eca_sl);
	double I_cabk = I_cabk_junc + I_cabk_sl;

	//  SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
	//  Vm-dependence modulation (with flagMica=1)
	double aRyR = 1.5; //  [-]
	double sRyR = 30; //  [mV]
	double pRyR = 0.065; //  [1/mV]
	double Vdep_SRCarel = flagMica * aRyR / (1 + exp((y[39] - sRyR) * pRyR)) + (1 - flagMica) * 1;

	double MaxSR = 15;
	double MinSR = 1;
	double kCaSR = MaxSR - (MaxSR - MinSR) / (1 + pow((ec50SR / y[31]), 2.5));
	double koSRCa = koCa / kCaSR;
	double kiSRCa = kiCa * kCaSR;
	double RI = 1 - y[14] - y[15] - y[16];
	ydot[14] = (kim * RI - kiSRCa * y[36] * y[14]) - (koSRCa * y[36] * y[36] * y[14] - kom * y[15]); //  R
	ydot[15] = (koSRCa * y[36] * y[36] * y[14] - kom * y[15]) - (kiSRCa * y[36] * y[15] - kim * y[16]); //  O
	ydot[16] = (kiSRCa * y[36] * y[15] - kim * y[16]) - (kom * y[16] - koSRCa * y[36] * y[36] * RI); //  I
	double J_SRCarel = Vdep_SRCarel * ks * y[15] * (y[31] - y[36]); //  [mM/ms]

	double J_Serca = pow(Q10SRCaP, Qpow) * Vmax_SRCaP * (pow((y[38] / Kmf), hillSRCaP) - pow((y[31] / Kmr), hillSRCaP))
	                 / (1 + pow((y[38] / Kmf), hillSRCaP) + pow((y[31] / Kmr), hillSRCaP));

	double J_SRleak = G_SRleak * (y[31] - y[36]); //  [mM/ms]

	//  Sodium and Calcium Buffering
	ydot[17] = kon_na * y[32] * (Bmax_Naj - y[17]) - koff_na * y[17]; //  NaBj      [mM/ms]
	ydot[18] = kon_na * y[33] * (Bmax_Nasl - y[18]) - koff_na * y[18]; //  NaBsl     [mM/ms]

	//  Cytosolic Ca Buffers
	ydot[19] = kon_tncl * y[38] * (Bmax_TnClow - y[19]) - koff_tncl * y[19];  //  TnCL      [mM/ms]
	ydot[20] = kon_tnchca * y[38] * (Bmax_TnChigh - y[20] - y[21]) - koff_tnchca * y[20]; //  TnCHc     [mM/ms]
	ydot[21] = kon_tnchmg * Mgi * (Bmax_TnChigh - y[20] - y[21]) - koff_tnchmg * y[21]; //  TnCHm     [mM/ms]
	ydot[22] = kon_cam * y[38] * (Bmax_CaM - y[22]) - koff_cam * y[22];       //  CaM       [mM/ms]
	ydot[23] = kon_myoca * y[38] * (Bmax_myosin - y[23] - y[24]) - koff_myoca * y[23]; //  Myosin_ca [mM/ms]
	ydot[24] = kon_myomg * Mgi * (Bmax_myosin - y[23] - y[24]) - koff_myomg * y[24]; //  Myosin_mg [mM/ms]
	ydot[25] = kon_sr * y[38] * (Bmax_SR - y[25]) - koff_sr * y[25];          //  SRB       [mM/ms]
	// J_CaB_cytosol = sum(ydot(19:25)); //  wrong formulation
	double J_CaB_cytosol = ydot[19] + ydot[20] + ydot[22] + ydot[23] + ydot[25];

	//  Junctional and SL Ca Buffers
	ydot[26] = kon_sll * y[36] * (Bmax_SLlowj - y[26]) - koff_sll * y[26]; //  SLLj      [mM/ms]
	ydot[27] = kon_sll * y[37] * (Bmax_SLlowsl - y[27]) - koff_sll * y[27]; //  SLLsl     [mM/ms]
	ydot[28] = kon_slh * y[36] * (Bmax_SLhighj - y[28]) - koff_slh * y[28]; //  SLHj      [mM/ms]
	ydot[29] = kon_slh * y[37] * (Bmax_SLhighsl - y[29]) - koff_slh * y[29]; //  SLHsl     [mM/ms]
	double J_CaB_junction = ydot[26] + ydot[28];
	double J_CaB_sl = ydot[27] + ydot[29];

	//  Ion concentrations
	//  SR Ca Concentrations
	ydot[30] = kon_csqn * y[31] * (Bmax_Csqn - y[30]) - koff_csqn * y[30]; //  Csqn [mM/ms]
	ydot[31] = J_Serca - (J_SRleak * Vmyo / Vsr + J_SRCarel) - ydot[30]; //  Ca_sr [mM/ms] // Ratio 3 leak current
	//  ydot[30] = 0;
	//  ydot[31] = 0;

	//  Sodium Concentrations
	double I_Na_tot_junc = I_Na_junc + I_nabk_junc + 3 * I_ncx_junc + 3 * I_nak_junc + I_CaNa_junc; //  [uA/uF]
	double I_Na_tot_sl = I_Na_sl + I_nabk_sl + 3 * I_ncx_sl + 3 * I_nak_sl + I_CaNa_sl; //  [uA/uF]
	// I_Na_tot_sl2 = I_Na_sl+I_nabk_sl+3*I_ncx_sl+3*I_nak_sl+I_CaNa_sl*0; //  [uA/uF]

	ydot[32] = -I_Na_tot_junc * Cmem / (Vjunc * Frdy) + J_na_juncsl / Vjunc * (y[33] - y[32]) - ydot[17];
	ydot[33] = -I_Na_tot_sl * Cmem / (Vsl * Frdy) + J_na_juncsl / Vsl * (y[32] - y[33])
	           + J_na_slmyo / Vsl * (y[34] - y[33]) - ydot[18]; // FluxNaSL=ydot[33];
	ydot[34] = J_na_slmyo / Vmyo * (y[33] - y[34]); //  [mM/msec]
	//  ydot[32] = 0;
	//  ydot[33] = 0;
	//  ydot[34] = 0;

	//  Potassium Concentration
	double I_K_tot = I_to + I_kr + I_ks + I_ki - 2 * I_nak + I_CaK + I_kp; //  [uA/uF]
	//  ydot[35] = -I_K_tot*Cmem/(Vmyo*Frdy); //  [mM/msec]
	ydot[35] = 0; //  -I_K_tot*Cmem/(Vmyo*Frdy); //  [mM/msec]

	//  Calcium Concentrations
	double I_Ca_tot_junc = I_Ca_junc + I_cabk_junc + I_pca_junc - 2 * I_ncx_junc; //  [uA/uF]
	double I_Ca_tot_sl = I_Ca_sl + I_cabk_sl + I_pca_sl - 2 * I_ncx_sl; //  [uA/uF]
	ydot[36] = -I_Ca_tot_junc * Cmem / (Vjunc * 2 * Frdy) + J_ca_juncsl / Vjunc * (y[37] - y[36])
	           - J_CaB_junction + (J_SRCarel) * Vsr / Vjunc + J_SRleak * Vmyo / Vjunc; //  Ca_j
	ydot[37] = -I_Ca_tot_sl * Cmem / (Vsl * 2 * Frdy) + J_ca_juncsl / Vsl * (y[36] - y[37])
	           + J_ca_slmyo / Vsl * (y[38] - y[37]) - J_CaB_sl; //  Ca_sl
	ydot[38] = -J_Serca * Vsr / Vmyo - J_CaB_cytosol + J_ca_slmyo / Vmyo * (y[37] - y[38]); //  [mM/msec]
	//  ydot[36] = 0;
	//  ydot[37] = 0;
	//  ydot[38] = 0;

	if (( flag_Ba  == 1 ) || (flag_EGTA == 1) ) {
		ydot[38] = 0;
	}
	if (flag_BAPTA == 1) {

		ydot[36] = 0;
		ydot[37] = 0;
		ydot[38] = 0;
	}


	double rate = (p_HR) * 1e-3;
	double I_app;
	if (fmod(t + 0, 1.0 / rate) <= 5)
		I_app = 9.5;
	else
		I_app = 0.0;
	double I_Na_tot = I_Na_tot_junc + I_Na_tot_sl;          //  [uA/uF]
	double I_Cl_tot = I_ClCa + I_Clbk;                      //  [uA/uF]
	double I_Ca_tot = I_Ca_tot_junc + I_Ca_tot_sl;          //  [uA/uF]
	double I_tot = I_Na_tot + I_Cl_tot + I_Ca_tot + I_K_tot;
	ydot[39] = -(I_tot - I_app);
	ydot[0] = 0.0;
	// vmax = ydot[39];
	// return 0;
}





//     //  Simulation type
//     switch lower(protocol)

//     case {'none', ''},
//          I_app = 0;

// case 'pace', //  pace w/ current injection at rate 'rate'
//         rate = (p_HR) * 1e-3;
//     if mod(t + 0, 1 / rate) <= 5
//         I_app = 9.5;
//     else
//         I_app = 0.0;
//     end

// case 'step', //  200-ms voltage step (from -80 to 10 mV) at rate 'rate'
//         rate = (p_HR) * 1e-3;
//     if mod(t, 1 / rate) < 10
//         V_clamp = -80;
//     elseif mod(t, 1 / rate) >= 10 && mod(t, 1 / rate) < 210
//     V_clamp = 10;
//     elseif mod(t, 1 / rate) >= 210
//     V_clamp = -80;
//     end
//     R_clamp = 0.02;
//     I_app = (V_clamp - y[39]) / R_clamp;

//     end

//  Membrane Potential

//  Adjust output depending on the function call
// if (nargin == 3)
//     output = ydot;
// elseif (nargin == 4) && strcmp(runType, 'ydot')
// output = ydot;
// elseif (nargin == 4) && strcmp(runType, 'rates')
// output = r;
// elseif (nargin == 4) && strcmp(runType, 'currents')
// // currents = [I_Catot I_Natot I_nabk I_nak I_kr I_ks I_kp I_tos I_tof I_ki I_ClCa I_Clbk I_ncx I_pca I_cabk];
// currents = [I_Catot I_Natot];
// output = currents;
// end
//  ----- END E-C COUPLING MODEL --------------
// }