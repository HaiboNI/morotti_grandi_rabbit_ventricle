#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>



#include "lsoda_C.hpp"
#include "Morotti_Grandi_Rabbit_Ventricle.cpp"




int main()
{
    //define constants

    int count = 0;

    // general parameters
    double dt = 0.1;
    double V, Vstep;

    double peak_current_pos, peak_current_neg, end_current;
    FILE *volt_out, *current_out;
    double I, t;
    int end_switch = 0;



    double state[68], ydot[68];
    // current_out = fopen("GB_default_current_trace.txt", "wt");
    //do voltage clamp and calculate current
    volt_out = fopen("GB_default_I-V_relationship.txt", "wt");

    InitialiseMorotti_Grandi_Rabbit_Ventricle(state);


    LSODA solver(67);
    double t_myofilament = 0.0;
    int counter = 0;

    for (t = 0; t < 10000; t += dt)
    {
        solver.lsoda(&Morotti_Grandi_Rabbit_Ventricle, state-1, &t_myofilament, t+dt, 0);
        counter ++;
        // Morotti_Grandi_Rabbit_Ventricle(t, state, ydot, 0);



        /*for (int i = 0; i < 67; ++i)
        {
            state[i] += dt * ydot[i];
        }*/
        if (counter % 100 == 0)
        {
            /* code */
            fprintf(volt_out, "%f %f %.10f\n", t, state[39], state[38] );
        }
    }


    return 0;
} // end main

